package app.config;

import app.access.PostgresUserRepository;
import app.access.UserRepository;
import app.service.UsersServiceImpl;
import app.service.UserConverter;
import app.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "app.security")
public class JavaConfiguration {

    @Bean
    public UserRepository userRepository() {
        return new PostgresUserRepository();
    }

    @Bean
    public UserService userService() { return new UsersServiceImpl(); }

    @Bean
    public UserConverter userConverter() { return new UserConverter(); }
}
