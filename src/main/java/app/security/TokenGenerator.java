package app.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class TokenGenerator {
    public String GetToken(Integer userId, String userName, String userRole) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("login", userName);
        claims.put("role", userRole);
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + 60*60*10000);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(String.valueOf(userId))
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, "verySecretCharacterSequence")
                .compact();
    }
}
