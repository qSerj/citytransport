package app.service;

import app.entities.dto.UserDto;
import app.entities.entity.User;
import org.springframework.stereotype.Service;

@Service
public class UserConverter {
    public User fromUserDtoToUser(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setEmail(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setRole(userDto.getRole());
        return user;
    }

    public UserDto fromUserToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .login(user.getEmail())
                .password(user.getPassword())
                .role(user.getRole())
                .build();
    }
}
