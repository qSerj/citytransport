package app.service;

import app.entities.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {
    UserDto createUser(String login, String password, String role);

    UserDto saveUser(UserDto user) throws ValidationException, UserNotExistsException;

    void deleteUser(Integer userId);

    UserDto findByLogin(String login);

    List<UserDto> findAll();

    Optional<UserDto> authenticate(String login, String password);
}
