package app.service;

import app.entities.dto.UserDto;
import app.entities.entity.User;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class AppUtils {

    private static int REDIRECT_ID = 0;

    private static final Map<Integer, String> id_uri_map = new HashMap<Integer, String>();
    private static final Map<String, Integer> uri_id_map = new HashMap<String, Integer>();

    public static void storeLoginedUser(HttpSession session, UserDto loginedUser) {
        session.setAttribute("loginedUser", loginedUser);
    }

    public static UserDto getLoginedUser(HttpSession session) {
        UserDto loginedUser = (UserDto) session.getAttribute("loginedUser");
        return loginedUser;
    }

    public static void storeLoginedUserJwt(HttpSession session, String token) {
        session.setAttribute("loginedUserJwt", token);
    }

    public static String getLoginedUserJwt(HttpSession session) {
        String jwt = (String) session.getAttribute("loginedUserJwt");
        return jwt;
    }

    public static int storeRedirectAfterLoginUrl(HttpSession session, String requestUri) {
        Integer id = uri_id_map.get(requestUri);

        if (id == null) {
            id = REDIRECT_ID++;
            uri_id_map.put(requestUri, id);
            id_uri_map.put(id, requestUri);
            return id;
        }

        return id;
    }

    public static String getRedirectAfterLoginUrl(HttpSession session, int redirectId) {
        String url = id_uri_map.get(redirectId);
        if (url != null) {
            return url;
        }
        return null;
    }
}
