package app.service;

import app.access.UserRepository;
import app.entities.dto.UserDto;
import app.entities.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsersServiceImpl implements UserService {
    @Autowired
    private UserRepository usersRepository;
    @Autowired
    private UserConverter usersConverter;

    @Override
    public UserDto createUser(String login, String password, String role) {
        User user = usersRepository.create(login, password, role);
        return usersConverter.fromUserToUserDto(user);
    }

    @Override
    public UserDto saveUser(UserDto user) throws UserNotExistsException {
        User savedUser = usersRepository.save(usersConverter.fromUserDtoToUser(user));
        return usersConverter.fromUserToUserDto(savedUser);
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public UserDto findByLogin(String login) {
        User users = usersRepository.findByLogin(login);
        if (users != null) {
            return usersConverter.fromUserToUserDto(users);
        }
        return null;
    }

    @Override
    public List<UserDto> findAll() {
        return usersRepository.findAll()
                              .stream()
                              .map(usersConverter::fromUserToUserDto)
                              .collect(Collectors.toList());
    }

    @Override
    public Optional<UserDto> authenticate(String login, String password) {
        User user = usersRepository.findByLogin(login);
        if (user.getPassword().equals(password)) {
            return Optional.of(usersConverter.fromUserToUserDto(user));
        }
        return Optional.empty();
    }
}
