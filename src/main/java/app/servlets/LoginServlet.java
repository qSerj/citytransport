package app.servlets;

import app.entities.dto.UserDto;
import app.security.TokenGenerator;
import app.service.AppUtils;
import app.service.UserNotExistsException;
import app.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.ContextLoaderListener;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/login")
@Slf4j
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private final UserService userService = ContextLoaderListener.getCurrentWebApplicationContext().getBean(UserService.class);
    private final TokenGenerator tokenGen = ContextLoaderListener.getCurrentWebApplicationContext().getBean(TokenGenerator.class);


    public LoginServlet() {
        super();
        log.info("constructor executed");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/views/loginView.jsp");

        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        UserDto userAccount = null;
        try {
            userAccount = userService.authenticate(userName, password).orElseThrow(() ->
                    new UserNotExistsException("Пользователь не существует или направильный пароль"));

            log.info("user '{}' logged in successesfuly", userName);

            AppUtils.storeLoginedUser(request.getSession(), userAccount);
            String token = tokenGen.GetToken(userAccount.getId(), userAccount.getLogin(), userAccount.getRole());
            AppUtils.storeLoginedUserJwt(request.getSession(), token);

            response.sendRedirect(request.getContextPath() + "/main.jsp");

        } catch (UserNotExistsException e) {
            log.warn("the user '{}' with password '{}' login FAILED", userName, password);

            String errorMessage = "Invalid userName or Password";

            request.setAttribute("errorMessage", errorMessage);

            RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/views/loginView.jsp");

            dispatcher.forward(request, response);
        }
    }
}
