package app.servlets;

import app.service.AppUtils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/jwt")
public class JwtTokenServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public JwtTokenServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String token = AppUtils.getLoginedUserJwt(request.getSession());
        PrintWriter writer = response.getWriter();
        writer.println(token);
    }
}
