package app.access;

import app.entities.entity.User;
import app.service.UserNotExistsException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserRepository {

    User create(String login, String password, String role);

    User save(User user) throws UserNotExistsException;

    void deleteById(Integer userId);

    List<User> findAll();

    User findByLogin(String login);
}
