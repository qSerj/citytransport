package app.access;

import app.entities.entity.User;
import app.service.UserNotExistsException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class PostgresUserRepository implements UserRepository {
    @Override
    public User create(String login, String password, String role) {
        Connection con = null;
        PreparedStatement stmt = null;
        int rowsAffected;
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/transport", "postgres", "123");
            stmt = con.prepareStatement("insert into people (login, password, role) values (?, ?, ?);");
            stmt.setString(1, login);
            stmt.setString(2, password);
            stmt.setString(3, role);
            rowsAffected = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
            }
            try {
                if (con != null) con.close();
            } catch (SQLException e) {
            }
        }
        return findByLogin(login);
    }

    @Override
    public User save(User user) throws UserNotExistsException {
        return null;
    }

    @Override
    public void deleteById(Integer userId) {
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/transport", "postgres", "123");
            stmt = con.prepareStatement("select * from people;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String login = rs.getString("login");
                String password = rs.getString("password");
                String role = rs.getString("role");
                users.add(new User(id, login, password, 0, BigDecimal.ZERO, role));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
            }
            try {
                if (con != null) con.close();
            } catch (SQLException e) {
            }
        }
        return users;
    }

    @Override
    public User findByLogin(String login) {
        User user = null;
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/transport", "postgres", "12345");
            stmt = con.prepareStatement("select * from people where login = ?;");
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                //String login = rs.getString("login");
                String password = rs.getString("password");
                String role = rs.getString("role");
                user = new User(id, login, password, 0, BigDecimal.ZERO, role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException e) {
            }
            try {
                if (con != null) con.close();
            } catch (SQLException e) {
            }
            return user;
        }
    }
}
