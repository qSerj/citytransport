package app.entities.entity;

import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static app.config.SecurityConfig.ROLE_ADMIN;
import static app.config.SecurityConfig.ROLE_USER;

@Data
@NoArgsConstructor
//@AllArgsConstructor
public class User {
    private Integer id;
    private String email;
    private String password;
    private Integer rentedTransportId;
    private BigDecimal balance;
    private String role;

    public User(Integer id, String email, String password, Integer rentedTransportId, BigDecimal balance, String role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.rentedTransportId = rentedTransportId;
        this.balance = balance;
        this.role = role;
    }
}
