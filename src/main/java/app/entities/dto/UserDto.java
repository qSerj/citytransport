package app.entities.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserDto {
    private Integer id;
    private String  login;
    private String  password;
    private String role;

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", login='" + login +
                '}';
    }
}
