<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Вход</title>
</head>
<body>

<h3>Введите данные учетной записи</h3>

<p style="color: red;">${errorString}</p>

<form method="POST" action="${pageContext.request.contextPath}/login">
  <input type="hidden" name="redirectId" value="${param.redirectId}" />
  <table border="0">
    <tr>
      <td>Имя пользователя:</td>
      <td><input type="text" name="userName" value= "${user.userName}" /> </td>
    </tr>
    <tr>
      <td>Пароль</td>
      <td><input type="password" name="password" value= "${user.password}" /> </td>
    </tr>

    <tr>
      <td colspan ="2">
        <input type="submit" value= "Вход" />
        <a href="${pageContext.request.contextPath}/">Отмена</a>
      </td>
    </tr>
  </table>
</form>

<p style="color:blue;">Войти можно, для примера, как:</p>

admin@transport.ru / 1234 <br>
petya@mail.ru / 1234

</body>
</html>
